let nodeGeocoder = require('node-geocoder');
 
let options = {
  provider: 'openstreetmap'
};
 
let geoCoder = nodeGeocoder(options);
geoCoder.reverse({lat:17.314751700000002,lon:74.16551699168082})
  .then((res)=> {
    console.log(res);
  })

  .catch((err)=> {
    console.log(err);
  });