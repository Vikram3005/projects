let nodeGeocoder = require('node-geocoder');
 
let options = {
  provider: 'openstreetmap'
};
 
let geoCoder = nodeGeocoder(options);
geoCoder.geocode('pimpri pune')
  .then((res)=> {
    console.log(res);
  })
  .catch((err)=> {
    console.log(err);
  });