let nodeGeocoder = require('node-geocoder');
 
let options = {
  provider: 'openstreetmap'
};
 
let geoCoder = nodeGeocoder(options);
// Reverse Geocode
  geoCoder.reverse({lat:18.5100423, lon:73.4265013})
  .then((res)=> {
    console.log(res);
  })
  .catch((err)=> {
    console.log(err);
  });