package com.spring.hibernate.test;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.spring.hibernate.bean.Employee;
import com.spring.hibernate.dao.EmployeeDao;

public class Test {
public static void main(String[] args) {
	
	
	//ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
	
	Resource rs = new ClassPathResource("applicationContext.xml");
	BeanFactory context = new XmlBeanFactory(rs);
	EmployeeDao dao=(EmployeeDao)context.getBean("d");
	
	Employee e=new Employee();
	e.setId(1);
	e.setName("vikramVishwas");
	e.setSalary(80000);
	
	dao.saveEmployee(e);
	//dao.updateEmployee(e);
}
}
