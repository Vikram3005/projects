var QRCode = require('qrcode')
 
 var opts = {
  errorCorrectionLevel: 'H',
  type: 'terminal',
  quality: 0.3,
  margin: 1,
  color: {
    dark:"#010599FF",
    light:"#FFBF60FF"
  }
}
  var segs = [
    { data: 'ABCDEFG', mode: 'alphanumeric' },
    { data: '0123456', mode: 'numeric' }
  ]
 	//toString(data,options,cb);
  QRCode.toString(segs,opts,function (err, string) {
    console.log(string)
  })

/*
 cb-callback function called after finish
 This is for Mixed/mannual mode selection.
 It is possible to manually specify each 
 segment with the relative mode

 toString(data,options);
output will print the QR Code image on console.
   
  */
