var QRCode = require('qrcode')
var opts = {
    errorCorrectionLevel: 'H',
    type: 'image/jpeg',
    quality: 0.3,
    margin: 1,
    color: {
      dark:"#010599FF",
      light:"#FFBF60FF"
    }
  }
 		 
var res = QRCode.create("mycode",opts);
					
console.log(res)  
/*
// QRCode object pattern
{
  modules,              // Bitmatrix class with modules data
  version,              // Calculated QR Code version
  errorCorrectionLevel, // Error Correction Level
  maskPattern,          // Calculated Mask pattern
  segments              // Generated segments
}
=================================================================================================================================================
create(data,options) it returns qrcode object,
output looks like as follows

{ modules:
   BitMatrix {
     size: 21,
     data:
      <Buffer 01 01 01 01 01 01 01 00 00 01 00 01 01 00 01 01 01 01 01 01 01 01 00 00 00 00 00 01 00 01 00 00 00 00 00 01 00 00 00 00 00 01 01 00 01 01 01 00 01 00 ... >,
     reservedBit:
      <Buffer 01 01 01 01 01 01 01 01 01 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 00 00 00 00 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 ... > },
  version: 1,
  errorCorrectionLevel: { bit: 2 },
  maskPattern: 1,
  segments:
   [ ByteData { mode: [Object], data: <Buffer 6d 79 63 6f 64 65> } ] }

*/
