
var QRCode = require('qrcode')
var data = [
    { data: 'ABCDEFG', mode: 'alphanumeric' },
    { data: '0123456', mode: 'numeric' }
  ]
var opts = {
    errorCorrectionLevel: 'H',
    type: 'image/jpeg',
    quality: 0.3,
    margin: 1,
    color: {
      dark:"#010599FF",
      light:"#FFBF60FF"
    },
    width:500
  }



function createQRCode(filename,data){

//toFile(filepath+filename,data,options_for_qrcode)

    QRCode.toFile("images/"+filename+".png",data,opts)
}

createQRCode("myQR",data) 

/*

using this API we can create QR code Images and store in specified path

*/
