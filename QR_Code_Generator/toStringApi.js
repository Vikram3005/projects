var QRCode = require('qrcode')
var opts = {
  errorCorrectionLevel: 'H',
  type: 'terminal',
  quality: 0.3,
  margin: 1,
  color: {
    dark:"#010599FF",
    light:"#FFBF60FF"
  }
}
var data = "This is QRCode"

 QRCode.toString(data,opts, function (err, string) {
  console.log(string)
})


/*
This will print the QR code on console

*/
