var mysql = require(`mysql`);
function createConnection(){

    var connection = mysql.createConnection({
        host : 'localhost',
        user :'root',
        password:'root',
        database:'javatech',
        multipleStatements:true
    })

    console.log('Connected to the Databases');
    
    return connection;
}
module.exports = {
    createConnection : createConnection
};