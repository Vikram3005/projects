const statusSuccess = 'success';
const statusFailure = 'failed';

function createResponse(status,data) {
    var response = {
        'status':status,
        'data' : data
    }
    return response;
}

module.exports = {
    success:statusSuccess,
    failed:statusFailure,
    createResponse:createResponse
}