var express = require(`express`);
var bodyParser = require(`body-parser`);
var users = require(`./user`);
var server = express();
server.use(bodyParser.json());
server.use(users);

server.listen(8080,'localhost',function () {
    console.log('server is running on port 8080...');
});