var express = require(`express`);
var utils = require(`./utils`);
var db = require(`./database`);

var router = express.Router();

/*1.api for user registration,if users phone number 
already registered then it will not accept same phone number. 
it will ask for new phone number. */

router.post("/API/register/", function (req, res) {
  var body = req.body;
  var connection = db.createConnection();
  var query = `select * from users where phone ='${body.phone}'`;
  var sql = `insert into users(name,age,gender,email,phone)
            values('${body.name}','${body.age}','${body.gender}','${body.email}','${body.phone}')`;
  connection.query(query, function (error, result) {
    if (error) {
      res.send(utils.createResponse(utils.failed, error));
    } else if (result.length > 0) {
      console.log("Query ok..");
      res.send(utils.createResponse( utils.failed,
          "phone number already exists,please try with new one.")
      );
    } else {
      connection.query(sql, function (error, result) {
        if (error) {
          res.send(utils.createResponse(utils.failed, error));
        } else {
          console.log("Query ok..");
          connection.end();
          res.send(utils.createResponse(utils.success, result));
        }
      });
    }
  });
});

/*2. here we get user profile details using his phone number*/

router.get("/API/profile", function (req, res) {
  var body = req.body;
  var connection = db.createConnection();
  var sql = `select * from users where phone='${body.phone}'`;
  connection.query(sql, function (error, result) {
    if (error) {
      res.send(utils.createResponse(utils.failed, error));
    }
    if (result.length == 0) {
      res.send(utils.createResponse(utils.failed,"phone number does not exists, you have to register first."));
    } else {
      console.log("Query ok..");
      connection.end();
      res.send(utils.createResponse(utils.success, result));
    }
  });
});

/*3.Api to update the user information*/

router.get("/API/edituser/:id", function (req, res) {
  var body = req.body;
  var connection = db.createConnection();
  var sql = `update users 
             set name='${body.name}',age='${body.age}',gender='${body.gender}',
             email='${body.email}',phone='${body.phone}' where id=${req.params.id}`;

  connection.query(sql, function (error, result) {
    if (error) {
      res.send(utils.createResponse(utils.failed, error));
    } else {
      console.log("Query ok..");
      connection.end();
      res.send(utils.createResponse(utils.success, result));
    }
  });
});

/*4. Api to delete existing user using user id*/

router.delete("/API/delete/:id", function (req, res) {
  var connection = db.createConnection();
  var query = `select * from users where id=${req.params.id}`;
  var sql = `delete from users where id=${req.params.id}`;

  connection.query(query, function (error, result) {
    if (error) {
      res.send(utils.createResponse(utils.failed, error));
    } else if (result.length == 0) {
      console.log("Query ok..");
      connection.end();
      res.send(utils.createResponse(utils.failed, "user does not exists"));
    } else {
      connection.query(sql, function (error, result) {
        if (error) {
          res.send(utils.createResponse(utils.failed, error));
        } else {
          console.log("Query ok..");
          connection.end();
          res.send(utils.createResponse(utils.success, result));
        }
      });
    }
  });
});

/*5. Here we get all the registered users */

router.get("/API/getAll", function (req, res) {
  var connection = db.createConnection();
  var sql = "select * from users";
  connection.query(sql, function (error, result) {
    if (error) {
      res.send(utils.createResponse(utils.failed, error));
    } else {
      console.log("Query ok...");
      connection.end();
      res.send(utils.createResponse(utils.success, result));
    }
  });
});

module.exports = router;
