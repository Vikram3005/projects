package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.servlet.dao.StudentDao;
import com.servlet.pojo.Student;

@WebServlet("/LoginStudent")
public class LoginStudent extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html");	
		PrintWriter out=resp.getWriter();	

		String email = req.getParameter("email");
		String password = req.getParameter("password");

		boolean status = StudentDao.getStudentByEmailAndPassword(email, password);

		if(status==false){
			out.print("<b>Invalid Email or Password...Try Again</b>");
			req.getRequestDispatcher("login.html").include(req, resp);
		}else if(status==true) {
			resp.sendRedirect("index.html");
		}	
		
		out.close();
	}
}
