package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;
import com.servlet.pojo.Student;

@WebServlet("/EditStudentSave")
public class EditStudentSave extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		int status = 0;
		String sid = req.getParameter("id");
		int id = Integer.parseInt(sid);
		String name=req.getParameter("name");
		
		String mark=req.getParameter("marks");
		int marks = Integer.parseInt(mark);
		
		String address=req.getParameter("address");
		String email=req.getParameter("email");
		String password = req.getParameter("password");
		
		Student s = new Student();
		s.setId(id);
		s.setName(name);
		s.setMarks(marks);
		s.setAddress(address);
		s.setEmail(email);
		s.setPassword(password);
		
		status = StudentDao.UpdateStudent(s);
		
		if(status > 0) {
			resp.sendRedirect("ViewStudents");
		//	out.print("Information Updated Successfully...");
		
		}else {
			out.print("sorry!!! Unable to Update Student Information...");
			req.getRequestDispatcher("EditStudentForm").include(req, resp);
		}
		out.close();
	}
}
