package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;

@WebServlet("/DeleteStudent")
public class DeleteStudent extends HttpServlet {

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		int status = 0;
		
		resp.setContentType("text/html");
		PrintWriter out	= resp.getWriter();
		
		String sid = req.getParameter("id");
		int id = Integer.parseInt(sid);
		
		status = StudentDao.DeleteStudent(id);
		
		if(status > 0) {
			resp.sendRedirect("ViewStudents");
		}else {
			
			out.print("Try again");
		}
		
		}
}
