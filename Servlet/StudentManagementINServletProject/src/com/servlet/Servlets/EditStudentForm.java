package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;
import com.servlet.pojo.Student;

@WebServlet("/EditStudentForm")
public class EditStudentForm extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		String sid = req.getParameter("id");
		int id = Integer.parseInt(sid);
		
		out.print("<h1>Edit Student Info Here</h1>");
		
		Student s = StudentDao.getStudentById(id);
		
	
		
		out.print("<body style='background-color: aqua;background-image:/school2.webp'>");
		out.print("<div class='container'>");
		out.print("<form action='EditStudentSave' method='post'>");
		out.print("<pre>");
	
		out.print("<input type='hidden' name='id' value='"+s.getId()+"'><br>");
		out.print("Name:      <input type='text' name='name'  value='"+s.getName()+"'><br>");
		out.print("Marks:     <input type='text' name='marks'  value='"+s.getMarks()+"'><br>");
		out.print("Address:   <input type='text' name='address'  value='"+s.getAddress()+"'><br>");
		out.print("Email:     <input type='email' name='email' value='"+s.getEmail()+"'><br>");
		out.print("Password:  <input type='password' name='password' value='"+s.getPassword()+"'><br>");
		out.print("<input type='submit' value='Edit & Save' style='background-color : green; color:black;'>");
		
		out.print("</pre>");
		out.print("</form>");
		out.print("</div>");
		out.print("</body>");
		
		out.close();
	}
}



