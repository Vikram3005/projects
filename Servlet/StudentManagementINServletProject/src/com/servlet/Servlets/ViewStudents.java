package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;
import com.servlet.pojo.Student;

@WebServlet("/ViewStudents")
public class ViewStudents extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html");
		PrintWriter out	= resp.getWriter();
		
	
		
		out.print("<html>");
		
		out.print("<head><title>info</title></head>");
		
		out.print("<h1>AllStudentList</h1>");
		out.print("<a href='index.html'>GO TO HOME</a><br><br><br>");
		out.print("<body style='background-color: aqua;'>");
		
		
		
		List<Student> list = StudentDao.getAllStudent();
		
		out.print("<table border='1' width='100%'>");
		out.print("<tr><th>Id</th><th>Name</th><th>Marks</th><th>Address</th><th>Email-Id</th><th>Edit</th><th>Delete</th></tr>");
		
		for (Student s : list) {
			out.print("<tr><td>"+s.getId()+"</td><td>"+s.getName()+"</td><td>"+s.getMarks()+"</td><td>"+s.getAddress()+
			"</td><td>"+s.getEmail()+"</td><td><a href='EditStudentForm?id="+s.getId()+"'>Edit</a><td><a href='DeleteStudent?id="+s.getId()+"'>Delete</a></tr>");
		}
		out.print("</table>");
		out.print("</body>");
		out.print("</html>");
		out.close();
	}	
}