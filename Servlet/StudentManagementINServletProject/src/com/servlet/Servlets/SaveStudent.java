package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;
import com.servlet.pojo.Student;

@WebServlet("/SaveStudent")
public class SaveStudent extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		String name = req.getParameter("name");
		String mrks = req.getParameter("marks");
		
		int marks = Integer.parseInt(mrks);
		
		String address = req.getParameter("address");
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		Student s = new Student();
		
		s.setName(name);
		s.setMarks(marks);
		s.setAddress(address);
		s.setEmail(email);
		s.setPassword(password);
		
		int status = StudentDao.SaveStudent(s);
		if(status > 0) {
			out.print("Student Saved Successfully...");
			req.getRequestDispatcher("login.html").include(req, resp);
		}else {
			out.print("Not Added Something went Wrong...");
		}
			out.close();
	}
}
