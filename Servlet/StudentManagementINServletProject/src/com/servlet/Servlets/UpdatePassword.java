package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.servlet.dao.StudentDao;

@WebServlet("/UpdatePassword")
public class UpdatePassword extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		int status = StudentDao.UpdatePassword(email, password);
		
		if(status > 0) {
			out.print("<b style='color: green;'>Password Updated Successfully Now You Can Login...</b>");
			req.getRequestDispatcher("login.html").include(req, resp);	
		}else {
			out.print("<b style='color: red;'>Incorrect Email-Id</b>");
			req.getRequestDispatcher("updatepassword.html").include(req, resp);
		}
	}
}
