package com.servlet.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LogoutStudent")
public class LogoutStudent extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
	     resp.setContentType("text/html");  
         PrintWriter out=resp.getWriter();  
           
         req.getRequestDispatcher("login.html").include(req, resp);  
           
         HttpSession session=req.getSession(true);  
         session.invalidate();  
           
         out.print("You are successfully logged out!");  
           
         out.close();
		
	}
	
}
