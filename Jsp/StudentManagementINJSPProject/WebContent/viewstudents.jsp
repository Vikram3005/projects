<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List</title>
<style>
td {
	text-align: center;
}

body { 
	background-color: aqua;
}

b {
	color: gray;
}
</style>
</head>
<body>

	<%@page import="com.jsp.Model.Student"%>
	<%@page import="com.jsp.Dao.StudentDao"%>
	<%@page import="java.util.*"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<h1>List Of All Students</h1>

	<%
		String email = (String) session.getAttribute("email");

		List<Student> list = StudentDao.getAllStudent();
		request.setAttribute("list", list);
		
		if(email==null){ 
			request.getRequestDispatcher("exception.jsp").include(request, response);
		}
		
	%>
	<b>Current User :</b><%=email%>

	<table border="1" width="90%">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Marks</th>
			<th>Address</th>
			<th>Email-ID</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${list}" var="s">
			<tr>
				<td>${s.getId()}</td>
				<td>${s.getName()}</td>
				<td>${s.getMarks()}</td>
				<td>${s.getAddress()}</td>
				<td>${s.getEmail()}</td>
				<td><a href="editstudentinfoform.jsp?id=${s.getId()}">Edit</a></td>
				<td><a href="deletestudent.jsp?id=${s.getId()}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<a href="index.html">Home</a>
</body>
</html>