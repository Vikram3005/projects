<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>EditInfo</title>
</head>
<body>
<%@page import="com.jsp.Dao.StudentDao"%>
<jsp:useBean id="s" class="com.jsp.Model.Student"></jsp:useBean>
<jsp:setProperty property="*" name="s"/>
<%
int status = StudentDao.UpdateStudent(s);

if(status > 0){
	response.sendRedirect("editstudent-success.jsp");
}
else{
	response.sendRedirect("editstudent-error.jsp");

}
%>
</body>
</html>