<%@page import="com.jsp.Dao.StudentDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>

	<%
String email = request.getParameter("email");
String password = request.getParameter("password");

session.setAttribute("email", email);


boolean status = StudentDao.getStudentByEmailAndPassword(email, password);
if(status){
	
	response.sendRedirect("login-success.jsp");
}else{
	response.sendRedirect("login-error.jsp");
}

%>
</body>
</html>