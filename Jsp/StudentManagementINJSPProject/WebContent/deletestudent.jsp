<%@page import="com.jsp.Dao.StudentDao"%>
<jsp:useBean id="s" class="com.jsp.Model.Student"></jsp:useBean>
<jsp:setProperty property="*" name="s" />
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Delete</title>
</head>
<body>

	<%
String sid = request.getParameter("id");
int id=Integer.parseInt(sid);
int status=StudentDao.DeleteStudent(id);
if(status > 0){
response.sendRedirect("deletestudent-success.jsp");
}else{
	response.sendRedirect("deletestudent-error.jsp");
}

%>

</body>
</html>