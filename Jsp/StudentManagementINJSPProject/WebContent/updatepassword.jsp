<%@page import="com.jsp.Model.Student"%>
<%@page import="com.jsp.Dao.StudentDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Password</title>
</head>
<body>

	<%
String email = request.getParameter("email");
String password = request.getParameter("password");
	
int status = StudentDao.UpdatePassword(email, password);
if(status > 0){
	response.sendRedirect("updatepassword-success.jsp");
}else{
	response.sendRedirect("updatepassword-error.jsp");
}

%>

</body>
</html>