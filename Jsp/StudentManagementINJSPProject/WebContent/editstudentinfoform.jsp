<%@page import="com.jsp.Model.Student"%>
<%@page import="com.jsp.Dao.StudentDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Edit Info</title>
<style>
body {
	background-image: url('school2.webp');
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}
h1{
color: orange;
}
a{
color: orange;
}
</style>
</head>
<body>
<%
int id = Integer.parseInt(request.getParameter("id"));
Student s = StudentDao.getStudentById(id);
		
%>

<h1> Edit Student Information Here</h1>

<form action="editstudent.jsp" method="post">
	<input type="hidden" name="id" value="<%=s.getId()%>"/>
		<pre>
Name :     <input type="text" name="name" required="required" value="<%=s.getName()%>"><br>
Marks :    <input type="text" name="marks" required="required" value="<%=s.getMarks()%>"><br>
Address :  <input type="text" name="address" required="required"
				value="<%=s.getAddress()%>"><br>
Email :    <input type="email" name="email" required="required" value="<%=s.getEmail()%>"><br>
Password : <input type="password" name="password" required="required"
				value="<%=s.getPassword()%>"><br>
<input type="submit" value="Edit & Save" style="background-color : green; color:black;">
</pre>
	</form>


</body>
</html>