<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LogOut</title>
</head>
<body>
	<%
		session = request.getSession(true);
		String email = (String) session.getAttribute("email");
		if (email == null) {
			request.getRequestDispatcher("exception.jsp").include(request, response);
		} else {
			response.sendRedirect("logout-success.jsp");
			session.removeAttribute("email");
			session.invalidate();
		}
	%>
</body>
</html>