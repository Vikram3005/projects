<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Success</title>
<style>
b {
	color: green;
}
</style>
</head>
<body>
	<b>Student Registration Successful...</b>
	<br>
	<jsp:include page='login.html'></jsp:include>
</body>
</html>