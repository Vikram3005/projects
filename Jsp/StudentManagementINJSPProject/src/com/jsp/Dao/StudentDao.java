package com.jsp.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jsp.Model.Student;

public class StudentDao {

	private static Connection connection = null;

	public static Connection getConnection() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost/SpringDB","root","root");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static int SaveStudent(Student s) {
		int i=0;

		try {

			connection = StudentDao.getConnection();
			String sql = "insert into studentjsp(name,marks,address,email,password)values(?,?,?,?,?)";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, s.getName());
			ps.setInt(2, s.getMarks());
			ps.setString(3, s.getAddress());
			ps.setString(4, s.getEmail());
			ps.setString(5, s.getPassword());

			i =	ps.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	public static int UpdateStudent(Student s) {
		int i=0;
		try {
			
			connection = StudentDao.getConnection();
			String sql = "update studentjsp set name=?,marks=?,address=?,email=?,password=? where id=?";
			PreparedStatement ps = connection.prepareStatement(sql);

			ps.setString(1, s.getName());
			ps.setInt(2, s.getMarks());
			ps.setString(3, s.getAddress());
			ps.setString(4, s.getEmail());
			ps.setString(5, s.getPassword());
			ps.setInt(6, s.getId());
			
			i=ps.executeUpdate();
			connection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	public static Student getStudentById(int id) {
		
		Student s = new Student();
		try {
			
			connection = StudentDao.getConnection();
			String sql = "select * from studentjsp where id=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				s.setId(rs.getInt(1));
				s.setName(rs.getString(2));
				s.setMarks(rs.getInt(3));
				s.setAddress(rs.getString(4));
				s.setEmail(rs.getString(5));
				s.setPassword(rs.getString(6));
			}

			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return s;
	}
	public static List<Student> getAllStudent() {
		
		List<Student> list = new ArrayList<Student>();
		try {

			connection = StudentDao.getConnection();
			String sql = "select * from studentjsp ";
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Student s = new Student();
				s.setId(rs.getInt(1));
				s.setName(rs.getString(2));
				s.setMarks(rs.getInt(3));
				s.setAddress(rs.getString(4));
				s.setEmail(rs.getString(5));
				s.setPassword(rs.getString(6));

				list.add(s);
			}

			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static int DeleteStudent(int id) {
		int i=0;
		try {

			connection = StudentDao.getConnection();
			String sql = "delete from  studentjsp where id=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			i =	ps.executeUpdate();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
	public static boolean getStudentByEmailAndPassword(String email,String password) {
		Student s = new Student();
		try {

			connection = StudentDao.getConnection();
			String sql = "select * from studentjsp where email=? and password=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2,password) ;
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return true;
			}

			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public static Student getStudentByEmail(String email) {
		Student s = new Student();
		try {

			connection = StudentDao.getConnection();
			String sql = "select * from studentjsp where email=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, email);
			
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				s.setId(rs.getInt(1));
				s.setName(rs.getString(2));
				s.setMarks(rs.getInt(3));
				s.setAddress(rs.getString(4));
				s.setEmail(rs.getString(5));
				s.setPassword(rs.getString(6));
				return s;
			}

			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static int UpdatePassword(String email,String password) {
		int i=0;
		try {
			
			connection = StudentDao.getConnection();
			String sql = "update studentjsp set password=? where email=?";
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, password);
			ps.setString(2, email);
			
			i=ps.executeUpdate();
			connection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}
}